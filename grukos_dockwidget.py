# -*- coding: utf-8 -*-
from __future__ import absolute_import

import subprocess
import webbrowser

# from PyQt5 import Qt
# from qgis.PyQt import Qt
from qgis.PyQt import QtGui, uic, QtCore
from qgis.PyQt.QtCore import pyqtSignal
from qgis.PyQt.QtGui import QCursor
from qgis.PyQt.QtWidgets import QApplication, QDockWidget, QTreeWidgetItemIterator

from .grukos_id import *
from .grukos_maptool import *


class GrukosDockWidget(QDockWidget):
    closingPlugin = pyqtSignal()

    # Self is of type GrukosDockWidget
    def __init__(self, qgis_iface, parent=None):
        super(GrukosDockWidget, self).__init__(parent)
        self.ui = uic.loadUi(os.path.join(os.path.dirname(__file__), 'grukos_dockwidget_base.ui'), self)

        GrukosAux.log_info('\n\n------------ GRUKOS ------------- GRUKOS ------------- GRUKOS ------------- GRUKOS')

        self.iface = qgis_iface
        self.setWindowTitle(f'{GrukosAux.get_meta_name()} {GrukosAux.get_meta_version()}')

        # Connect signals to slots
        self.lineEditSearchByNo.returnPressed.connect(self.search_by_alphanumeric)
        self.pushButtonAlphaSearch.clicked.connect(self.search_by_alphanumeric)
        self.pushButtonSearchBySelection.clicked.connect(self.search_by_selection)
        self.pushButtonSearchbyDrawingAPolygon.clicked.connect(self.search_by_rectangle)
        self.pushButtonClearResult.clicked.connect(self.clear_listView_clicked)

        self.checkBoxExpand.clicked.connect(self.expand_treeitems_event)
        self.checkBoxSelectAll.clicked.connect(self.select_treeitems_event)

        self.treeWidget_addLayer.doubleClicked.connect(self.tree_addLayer_clicked)
        self.treeWidget_addLayer.expandAll()

        self.listView_result.doubleClicked.connect(self.listView_clicked)

        # Check database connectivity
        db = GrukosDb()
        GrukosAux.log_info('GRUKOS version: {}'.format(GrukosAux.get_meta_version()))
        GrukosAux.log_info('Userole: {}'.format(GrukosDb.get_userrole()))
        GrukosAux.log_info('Test DB connection: {} (0=no connection, 1=connection)'.format(db.test_connection()))
        GrukosAux.log_info('PostgreSQL version: {}'.format(db.get_version()))

        # Rebuild resources.qrc to py whenever an icon is changed or added:
        # From the OSGeo4W Shell, run qt5_env.bat and py3_env.bat. Then, change the directory to plugin root and run:
        # pyrcc5 -o resources.py resources.qrc

        icon1 = QtGui.QIcon()
        icon1.addPixmap(QtGui.QPixmap(":/plugins/grukos/icons/magnifier.png"))
        self.pushButtonAlphaSearch.setIcon(icon1)

        icon2 = QtGui.QIcon()
        icon2.addPixmap(QtGui.QPixmap(":/plugins/grukos/icons/selection_search.png"))
        self.pushButtonSearchBySelection.setIcon(icon2)

        icon3 = QtGui.QIcon()
        icon3.addPixmap(QtGui.QPixmap(":/plugins/grukos/icons/bound_search.png"))
        self.pushButtonSearchbyDrawingAPolygon.setIcon(icon3)

        self.expand_treeitems(True)

    def expand_treeitems_event(self):
        checked = self.checkBoxExpand.isChecked()
        self.expand_treeitems(not checked)

    def expand_treeitems(self, expand=False):
        it = QTreeWidgetItemIterator(self.treeWidget_addLayer, QTreeWidgetItemIterator.All)
        while it.value():
            item = it.value()
            # Do not set checkboxes for group (parent) items
            if item.parent():
                item.parent().setExpanded(expand)

            it += 1

    def select_treeitems_event(self):
        checked = self.checkBoxSelectAll.isChecked()
        self.select_treeitems(checked)

    def select_treeitems(self, checked):
        """ This is actually possibly in QtDesigner """
        # Uncheck items in treeWidget_addLayer
        it = QTreeWidgetItemIterator(self.treeWidget_addLayer, QTreeWidgetItemIterator.All)
        while it.value():
            item = it.value()
            # Do not set checkboxes for group (parent) items
            if item.parent() is not None:
                # GrukosAux.log_info(f'item.parent: **** {item.parent().text(0)} ****')
                # GrukosAux.log_info(f'item: {item.text(0)}')

                if checked:
                    item.setCheckState(0, QtCore.Qt.Checked)
                else:
                    item.setCheckState(0, QtCore.Qt.Unchecked)

            # print unicode(item.text(0))
            it += 1

    def hide_experimental_layers_in_tree(self):
        """ Toggle visibility of raster layers in treeview """
        try:
            it = QTreeWidgetItemIterator(self.treeWidget_addLayer, QTreeWidgetItemIterator.All)
            while it.value():
                item = it.value()
                # Do not check parent items (group text)
                if item.parent() is not None:
                    if 'raster' in item.text(0):
                        item.setHidden(True)
                it += 1
        except Exception as e:
            QApplication.restoreOverrideCursor()
            GrukosAux.log_error('hide_experimental_layers_in_tree: {}'.format(e))

    def get_dbchecked_layernames(self, get_gui_layers=False):
        def get_all_checked_layers():
            """ Get a list of layers checked by the user """
            it = QTreeWidgetItemIterator(self.treeWidget_addLayer, QTreeWidgetItemIterator.All)
            layers = []
            while it.value():
                item = it.value()
                # Do not check parent items (group text)
                if item.parent() is not None:
                    if item.checkState(0) == QtCore.Qt.Checked:
                        layers.append(item.text(0))
                it += 1

            return layers

        """ Return list of database layers checked in gui """
        gui_layers = get_all_checked_layers()

        if get_gui_layers:
            return gui_layers

        qgrukos = GrukosQgis(self.iface)
        db_layernames = qgrukos.guilayername2dblayername(gui_layers)

        # print 'gui_layers checked: ', gui_layers
        # print 'db_layers checked: ', db_layers

        return db_layernames

    def listView_clicked(self, model_view):
        """
             Add search result to map or open deeplink:
               If table is not open, open it and zoom to gkoids.
               If table is open just zoom to gkoids.
               model_view: QModelIndex
        """
        # Handle any non GRUKOS tables like external url or internal folders
        itemtext = model_view.data()  # ModelDB (320555 - Langeland)
        linktype = itemtext.split(' ')[0].strip()  # ModelDB

        # GrukosAux.msg_box('itemtext: {}'.format(itemtext))
        # GrukosAux.msg_box('linktype: {}'.format(linktype))

        gkoid_kortnavn = itemtext.replace(linktype, '').strip()[1:-1]  # 320555 - Langeland
        # GrukosAux.msg_box('gkoid_kortnavn: {}'.format(gkoid_kortnavn))

        gkoid = gkoid_kortnavn.split(' ')[0]
        # GrukosAux.msg_box('gkoid: {}'.format(gkoid))

        kortnavn = gkoid_kortnavn.split(' ')[2]  # replace(gkoid + ' - ', '').trim()
        # GrukosAux.msg_box('kortnavn: {}'.format(kortnavn))

        tablename = linktype
        # GrukosAux.msg_box('tablename: {}'.format(tablename))

        if linktype.lower() in ['deeplinkgeoscene', 'deeplinkmodeldb', 'deeplinkstyrelse', 'deeplinkrefbib']:

            db = GrukosDb()

            if linktype == 'deeplinkgeoscene':
                link_record = db.gko_linksearch(linktype, gkoid=gkoid)
                gsmod_projectfile = link_record[0][1]
                os.startfile(gsmod_projectfile)
            elif linktype == 'deeplinkmodeldb':
                link_record = db.gko_linksearch(linktype, gkoid=gkoid)
                url = link_record[0][1]
                ie = webbrowser.get()
                ie.open(url)
            elif linktype == 'deeplinkstyrelse':
                link_record = db.gko_linksearch(linktype, gkoid=gkoid)
                url = link_record[0][1]
                ie = webbrowser.get()
                ie.open(url)
            elif linktype == 'deeplinkrefbib':
                link_record = db.gko_linksearch(linktype, gkoid=gkoid)
                folder = link_record[0][1]
                if os.path.exists(folder):
                    subprocess.Popen(r'explorer "{}", shell=True'.format(folder))
                else:
                    GrukosAux.log_error('Mappen {} findes ikke. Kontakt GRUKOS administrator'.format(folder))
                    GrukosAux.msg_box('Mappen: {} findes ikke.\nKontakt GRUKOS administrator!'.format(folder))
                    # Fall back on parent ref bib dir
                    subprocess.Popen(r'explorer "{}", shell=True'.format('F:\GKO\Ref_Bibliotek'))
            else:
                GrukosAux.msg_box('MST: Non valid {}'.format(itemtext.lower()))
        else:
            # Handle GRUKOS table
            QApplication.setOverrideCursor(QCursor(Qt.WaitCursor))

            try:
                # table = model_view.data()
                # GrukosAux.msg_box('table: {}'.format(table))

                qgrukos = GrukosQgis(self.iface)
                added, gkoid = qgrukos.add_layer_to_map(gkoid=gkoid, tablename=tablename, kortnavn=kortnavn)

                if added:
                    gkoids = [gkoid]
                    toc_name = f'{tablename} ({gkoid} - {kortnavn})'
                    qgrukos.zoom_to_gkoids(gkoids, toc_name)
                    GrukosAux.log_info(f'listView_clicked gko added: {toc_name}')

                    # Special case where user might have searched for plant (anl_id or anl_navn)
                    # If so select the actual plant iol_alle since every thing is base on gkoid
                    if tablename.lower() == 'iol_alle':
                        searchstr = self.lineEditSearchByNo.text()
                        selected = qgrukos.select_plant(tablename, searchstr)
                else:
                    GrukosAux.log_info(f'listView_clicked gko NOT added: {tablename}')

            except Exception as e:
                QApplication.restoreOverrideCursor()
                GrukosAux.log_error(f'listView_clicked exception: {e}')
                GrukosAux.msg_box(f'MST fejl {e}')

            QApplication.restoreOverrideCursor()

    def tree_addLayer_clicked(self, model_index):
        """ Treewidget doubleclick signal - Add all records from choosen layer to map """
        try:
            columnindex = 0
            selected_layer = self.treeWidget_addLayer.currentItem().text(columnindex)

            # GrukosAux.msg_box('selectedlayer: {}'.format(selected_layer))

            if selected_layer.lower() in ['geoscene3d', 'modeldb', 'reference bibliotek', 'mst.dk/gko rapportliste']:
                GrukosAux.msg_box('Linklag kan kun åbnes via en søgning fra søgeresultatlisten')
                return

            QApplication.setOverrideCursor(QCursor(Qt.WaitCursor))

            ui_qgis = GrukosQgis(self.iface)
            ui_qgis.add_layer_to_map(tablename=selected_layer, filter=False)
        except Exception as e:
            QApplication.restoreOverrideCursor()
            GrukosAux.log_error(f'MST listView_clicked exception: {e}')
            GrukosAux.msg_box(f'MEST fejl {e}')

        QApplication.restoreOverrideCursor()

    # def search_by_anlaeg_no(self, event):
    #     """ Search by anlæg no - translate to gkoid - pass on"""
    #     if len(self.lineEditSearchByNo.text()) == 0:
    #         msg = 'Der er ikke indtastet et anlægnummer!'
    #         GrukosAux.log_info(msg)
    #         GrukosAux.msg_box(msg)
    #         return
    #
    #     '''
    #     if not self.lineEditSearchByNo.text().isdigit:
    #         msg = 'Der er ikke indtastet et tal som anlægnummer!'
    #         GrukosAux.log_info(msg)
    #         GrukosAux.msg_box(msg)
    #         return
    #     '''
    #
    #     anlaegsno = self.lineEditSearchByNo.text()
    #     GrukosAux.log_info('-----> Søgning via anlægsnr: {}'.format(anlaegsno))
    #
    #     db = GrukosDb()
    #     gkoid, sql = db.gko_get_gkoid(anlaegsno)
    #
    #     if gkoid == '':
    #         msg = 'Der er ikke fundet anlæg med id: {}'.format(anlaegsno)
    #         GrukosAux.log_info(msg)
    #         GrukosAux.msg_box(msg)
    #         return
    #
    #     self.search_gkoid_or_kortnavn(gkoid)

    # def search_by_no(self, event):
    #     """  User initialized search event. Search all tables in db by gkoid or kortnavn """
    #     # Split link_layers and db_layers from tree-layers
    #     #link_layers = [d for d in tree_layers if 'deeplink' in d]
    #     #db_layers = [d for d in tree_layers if 'deeplink' not in d]
    #
    #     # Get user input
    #     if len(self.lineEditSearchByNo.text()) == 0:
    #         msg = u'Der er ikke indtastet et kortlægningsnummer eller -navn!'
    #         GrukosAux.msg_box(msg)
    #         return
    #
    #     self.search_gkoid_or_kortnavn(self.lineEditSearchByNo.text())

    def search_by_alphanumeric(self):
        """Generic search controlled by search definitions in search_def.json"""
        if len(self.lineEditSearchByNo.text()) == 0:
            return

        searchstr = self.lineEditSearchByNo.text()

        # What ever entered it needs to be converted to a gkoid according to json template ...
        db = GrukosDb()
        list_gkoids = db.gko_generic_search(searchstr)  # now a set not list

        if not list_gkoids:
            # self.listView_result.model().setStringList(QStringList{}) #TODO clear qlistview here
            msg = u'Søgningen giver ikke noget resultat'
            GrukosAux.msg_box(msg)
            return

        str_of_gkoids = ','.join(map(str, list_gkoids))

        # GrukosAux.log_info(f'list_gkoids-----> {list_gkoids}')
        # GrukosAux.log_info(f'str_of_gkoids----->{str_of_gkoids}')

        self.search_gkoid_or_kortnavn(str_of_gkoids)

    def search_gkoid_or_kortnavn(self, searchstr):
        """ Common method for gkoid, navn and anlaegsid search
        :type searchstr: object
        """
        GrukosAux.log_info(f'searchstr: {searchstr}')

        model = self.clear_result_model()

        tree_layers = self.get_dbchecked_layernames()
        if len(tree_layers) == 0:
            GrukosAux.msg_box("Der er ikke valgt nogle temaer eller links fra GRUKOS tabel-listen.")
            return

        gid = GrukosId()
        inputs = gid.load(searchstr)
        # searchstr:    input from ui like 320555 | langeland | lan%.
        # inputs:       [(320555, 'Langeland')]

        if not inputs:
            GrukosAux.msg_box(u'Kortnummer eller -navn findes ikke!')
            return

        GrukosAux.log_info('-----> Søgning via: {}'.format(inputs))
        QApplication.setOverrideCursor(QCursor(Qt.WaitCursor))

        # Query db for layers with either gkoid or kortnavn
        try:
            table_result = []
            db = GrukosDb()
            for gkoid, kortnavn in inputs:
                GrukosAux.log_info('gkoid: {}, kortnavn: {}'.format(gkoid, kortnavn))

                tables_found = db.gko_partsearch_db(tree_layers, gkoid=gkoid)  # just an array of tables
                GrukosAux.log_info('tables_found: {}'.format(tables_found))

                if not tables_found:
                    continue

                # GrukosAux.log_info('gkoid={}, kortnavn={} -> tables_found: '.format(gkoid, kortnavn, tables_found))
                tables_found_formated = []

                for t in tables_found:
                    tables_found_formated.append(f'{t} ({gkoid} - {kortnavn})')

                table_result += tables_found_formated

            # Add tables to listView
            if len(table_result) > 0:
                if len(table_result) > 0:
                    for t in table_result:
                        item = QtGui.QStandardItem(t)

                        # GrukosAux.msg_box('t: {}'.format(t))

                        model.appendRow(item)
                        self.listView_result.setModel(model)
            else:
                QApplication.restoreOverrideCursor()
                self.clear_result_model()  # TODO virker ikke
                GrukosAux.msg_box(u'Søgningen giver ingen resultater.')

            QApplication.restoreOverrideCursor()
        except Exception as e:
            QApplication.restoreOverrideCursor()
            GrukosAux.log_error('MST search_by_no exception: {}'.format(e))
            GrukosAux.msg_box('MST fejl: {}'.format(e))

    def search_by_selection(self, event):
        """  Search by selection """
        try:
            GrukosAux.log_info('Søgning via valg i kort')

            layer = self.iface.activeLayer()
            if layer is None:
                msg = u'Der skal vaere valgt et lag i lagkontrollen!'
                GrukosAux.msg_box(msg)
                return

            if len(layer.selectedFeatures()) == 0:
                layername = layer.name()
                msg = u'Der er ingen valgte geometrier i tema "{}".'.format(layername)
                GrukosAux.msg_box(msg)
                return

            gkoids = []
            for ft in layer.selectedFeatures():
                gkoids.append(str(ft['gkoid']))

            gkoids_unique = list({str(s): s for s in gkoids}.values())
            gkoids_unique_comma_str = ','.join([str(s) for s in gkoids_unique])

            self.search_gkoid_or_kortnavn(gkoids_unique_comma_str)

            QApplication.restoreOverrideCursor()

        except Exception as e:
            QApplication.restoreOverrideCursor()
            GrukosAux.log_error('MST search_by_selection exception: {}'.format(e))
            GrukosAux.msg_box('MST search_by_selection exception: {}'.format(e))

    def search_by_rectangle(self, event):
        """ Search by user invoked rectangular maptool """
        try:
            GrukosAux.log_info(str('Rektangelsøgning'))
            self.clear_result_model()  # TODO not working ...
            self.lineEditSearchByNo.setText('')  # spatial search does not use gkoid or kortnavn so delete any text

            QApplication.setOverrideCursor(QCursor(Qt.WaitCursor))

            self.rect_tool = RectangleMapTool(self.iface, self.listView_result, self.treeWidget_addLayer)
            self.iface.mapCanvas().setMapTool(self.rect_tool)
            # TEST: self.rect_tool.canvasClicked.connect(self.canvas_clicked)

            QApplication.restoreOverrideCursor()

        except Exception as e:
            QApplication.restoreOverrideCursor()
            GrukosAux.log_error('MST search_by_rectangle exception: {}'.format(e))
            GrukosAux.msg_box('MST fejl: {}'.format(e))

    # def canvas_clicked(self, point, mousebutton):
    # TODO try to remove working code from grukos_maptool to grukos_qgis
    # QMessageBox.information(None, "Clicked coords", " x: " + str(point.x()) + " Y: " + str(point.y()))

    def closeEvent(self, event):
        self.closingPlugin.emit()
        event.accept()

    def clear_result_model(self):
        """ Clear the listView and return the model """
        model = QtGui.QStandardItemModel(self.listView_result)
        if not model:
            GrukosAux.msg_box('MST error: Cannot reference model in clear_result_model.')
            return

        # Above model wont clear
        model2 = self.listView_result.model()
        if model2 and model2.rowCount() > 0:
            model2.removeRows(0, model.rowCount())

        return model

    def clear_listView_clicked(self, event):
        """ Clear items in listView - user event"""
        model = self.listView_result.model()
        if model and model.rowCount() > 0:
            model.removeRows(0, model.rowCount())
