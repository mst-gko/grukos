# -*- coding: utf-8 -*-

from __future__ import absolute_import
from builtins import str
from builtins import object

from PyQt5.QtWidgets import QApplication
from qgis.core import QgsExpression, QgsFeatureRequest, QgsDataSourceUri, QgsVectorLayer, \
    QgsProject, QgsLayerTreeLayer, QgsLayerTreeGroup, QgsRasterLayer, QgsContrastEnhancement

from qgis.PyQt import QtGui
from qgis.PyQt.QtXml import QDomDocument

from .grukos_db import *
from .grukos_aux import *

"""  Class handling qgis interaction in grukos """


class GrukosQgis(object):
    def __init__(self, iface):
        self.iface = iface

        # Map the difference between treewidget layername and db tablename
        # For tables: Key = treewidget layername, Value = db tablename
        # For links: Key = treewidget name, Value = db tablename
        self.layermapping = {}

        # Modelområder (items are attributes in modelomr table):
        self.layermapping[u'Geologisk'] = 'modelomrgeologisk'
        self.layermapping[u'Geokemisk'] = 'modelomrgeokemisk'
        self.layermapping[u'Hydrologisk'] = 'modelomrhydrologisk'
        self.layermapping[u'Hydrostratigrafisk'] = 'modelomrhydrostratigrafisk'

        # Afgrænsninger
        self.layermapping[u'Alle IOL (både inden- og udenfor OSD)'] = 'iol_alle'

        # Gældende bekendtgøresle
        self.layermapping[u'Indsatsområde'] = 'io'  # Indsatsopråder, type: { NFI | SFI }
        self.layermapping[u'Indvindingsoplande uden for OSD'] = 'ioluosd'  # Indvindingsoplande uden for OSD
        self.layermapping[u'Drikkevandsinteresser'] = 'di'  # Drikkevandsinteresser, type { OD | OSD }
        self.layermapping[u'Følsommeindvindingsområder'] = 'fi'  # FølsommeindvindingsOmråder, type: { nfi | sfi }

        # Magasin og lertykkelse
        self.layermapping[
            u'Lertykkelse [pktgrid]'] = 'lertykkelse_pktgrid'  # { uspecificeret lerlag | enkelt lerlag | akkumuleret lerlag }
        self.layermapping[
            u'Lertykkelse [poly]'] = 'lertykkelse_poly'  # { uspecificeret | enkelt lerlag | akkumuleret lerlag }
        self.layermapping[
            u'Reduceret lertykkelse [pktgrid]'] = 'reduceret_lertykkelse_pktgrid'  # { uspecificeret | enkelt lerlag | akkumuleret lerlag }
        self.layermapping[
            u'Reduceret lertykkelse [poly]'] = 'reduceret_lertykkelse_poly'  # { uspecificeret | enkelt lerlag | akkumuleret lerlag }
        self.layermapping[u'Magasinlag [pkt]'] = 'magasinlag_pktgrid'
        self.layermapping[u'Magasinlag [lin]'] = 'magasinlag_lin'
        self.layermapping[u'Magasinlag [poly]'] = 'magasinlag_poly'
        self.layermapping[u'Magasinlag topkote [raster]'] = 'magasinlag_topkote_raster'
        self.layermapping[u'Magasinlag bundkote [raster]'] = 'magasinlag_bundkote_raster'
        self.layermapping[u'Magasinlag tykkelse [raster]'] = 'magasinlagtykkelse_raster'

        # Sårbarhed
        self.layermapping[u'Afgrænsningspolygon'] = 'afgpolygon'
        self.layermapping[u'Nitratsårbarhed'] = 'saarbarhed_nitrat'
        self.layermapping[u'Redoxgrænsen [pkt]'] = 'saarbarhed_redox_pkt'
        self.layermapping[u'Redoxgrænsen [poly]'] = 'saarbarhed_redox_poly'

        # Potentialkort og pejlinger
        self.layermapping[u'Pejlinger'] = 'pejlinger'
        self.layermapping[u'Potentialekort [pkt]'] = 'potentiale_pkt'  # { Magasinspecifik | Uspecificeret magasin }
        self.layermapping[u'Potentialekort [lin]'] = 'potentiale_lin'  # { Magasinspecifik | Uspecificeret magasin }
        self.layermapping[u'Potentialekort [poly]'] = 'potentiale_poly'  # { Magasinspecifik | Uspecificeret magasin }

        # Links
        self.layermapping[u'GeoScene3D'] = 'deeplinkgeoscene'
        self.layermapping[u'ModelDB'] = 'deeplinkmodeldb'
        self.layermapping[u'mst.dk/gko rapportliste'] = 'deeplinkstyrelse'
        self.layermapping[u'Reference bibliotek'] = 'deeplinkrefbib'

        # Ikke geografiske tabeller
        self.layermapping[u'Projekttabel'] = 'gkoprojekt'
        self.layermapping[u'NIRAS kommentar'] = 'metadata_niras'
        self.layermapping[u'Rambøll kommentar'] = 'metadata_ramboell'

        # Sandbox
        self.layermapping[u'Sandbox [pkt]'] = 'sandbox_pkt'
        self.layermapping[u'Sandbox [lin]'] = 'sandbox_lin'
        self.layermapping[u'Sandbox [poly]'] = 'sandbox_poly'

    def guilayername2dblayername(self, gui_layers):
        """ Convert list of gui layers to list of db layers """
        db_layers = []
        for lyr in gui_layers:
            db_layers.append(self.layermapping[lyr])

        return db_layers

    def add_raster_layer_to_map(self, selected_layer):
        GrukosAux.log_info('Raster selected_layer: {}'.format(selected_layer))
        tablename = self.layermapping[str(selected_layer)]
        GrukosAux.log_info('Raster tablename: {}'.format(tablename))
        connString = "PG: dbname=grukos host=10.33.131.50 user=grukosadmin password=merkel30rblaser port=5432 mode=2 schema=gvkort column=rast table={}".format(
            tablename)
        layer = QgsRasterLayer(connString, selected_layer)

        if layer.isValid():
            GrukosAux.log_info('Raster layer is valid')
            layer.setContrastEnhancement(QgsContrastEnhancement.StretchToMinimumMaximum)

        # QgsMapLayerRegistry.instance().addMapLayer(layer)
        QgsProject.instance().addMapLayer(layer)

    def add_layer_to_map(self, gkoid='', tablename='', kortnavn='', filter=True):
        """ Add layer chosen from treewidget or resultlistview to map
        :param gkoid: gkoid
        :param filter: Default true, then filter is postfix in parentese to tablename. If no filter the entire layer is loaded.
        :param tablename: Layer to add to map. GUI name is used not the database tablename
        :param filter: Use a filter boolean
        :return: True if layer added, otherwise false
        """

        if 'raster' in tablename:
            self.add_raster_layer_to_map(tablename)
            return

        db = GrukosDb()
        params = db.get_dbparams()

        if filter:
            # table = table_select.split(' ')[0].strip()  # pick xxx from 'xxx (123456)' or 'xxx (Langeland)'
            GrukosAux.log_info(f'add_layer_to_map.table: {tablename}, gkoid: {gkoid}')

            if not gkoid.isnumeric():
                gkoid = db.gko_kortnavn_to_gkoid(gkoid)  # It must be a kortlaegningsnavn
                GrukosAux.log_info('gkoid: {}'.format(gkoid))
            table = tablename
        else:
            table = tablename
            gkoid = -9999  # not used and zero fail below check on valid gkoid

        if not table or not gkoid:
            GrukosAux.log_error(f'add_layer_to_map.kortnavn: exception {gkoid}')
            GrukosAux.msg_box(f'Tabel: {table}, eller gkoid: {gkoid} er forkert!')
            return False

        # Prepare connection to database
        uri = QgsDataSourceUri()
        uri.setConnection(params['host'], str(params['port']), params['dbname'], params['user'],
                          params['password'])  # set host name, port, database name, username and password

        # Translate GUI treeview layername to db tablename
        tablename = ''
        try:
            if filter:
                tablename = table
            else:
                tablename = self.layermapping[str(table)]

            # Parse None as geometry for non mapable layers
            is_nongeographic_layer = False
            if tablename in ['gkoprojekt', 'metadata_niras', 'metadata_ramboell']:
                is_nongeographic_layer = True

        except KeyError:
            GrukosAux.msg_box('Laget: "{}" er ikke implementeret endnu'.format(table))
            GrukosAux.log_error('Key "{}" error for selected layer "{}", with gkoid: {}'.format(table, table, gkoid))
            return False

        # Check if user wants to add existing map layer again
        # TODO Hydrologisk becomes modelomrhydrologisk thats does not get caught in is_layer_open
        if self.is_layer_open(tablename):
            QApplication.restoreOverrideCursor()
            if not GrukosAux.msg_box_yes_no('Laget {} er allerede åbent.\nVil du åbne det igen.'
                                            '\n\nEr du i tvivl tryk nej'.format(tablename)):
                return None, None  # True  # Still zoom to gkoid, see caller

        if filter:
            if is_nongeographic_layer:
                uri.setDataSource("gvkort", tablename, None, "gkoid={}".format(gkoid))
            else:
                # Set database schema, table name, geometry column and optionally subset (WHERE clause)
                uri.setDataSource("gvkort", tablename, "geom", "gkoid={}".format(gkoid))
        else:
            if is_nongeographic_layer:
                uri.setDataSource("gvkort", tablename, None)
            else:
                # all views in database has an unique id column - pseudo primary key
                if tablename in ['vw_fremdriftstyringsvaerktoej', 'vw_indmeldinger']:
                    uri.setDataSource("gvkort", tablename, "geom", aKeyColumn='id')
                    uri.setKeyColumn('id')
                else:
                    uri.setDataSource("gvkort", tablename, "geom")

        # Check layer validity
        toc_name = tablename
        if filter:
            toc_name = '{} ({} - {})'.format(tablename, gkoid, kortnavn)
        vlayer = QgsVectorLayer(uri.uri(), toc_name, "postgres")  # loadDefaultStyleFlag=True
        if not is_nongeographic_layer:
            if not vlayer.isValid():
                # A default next val sequence and reader and writer grants must be set
                # As of QGIS 3.10 the srid must be specified with geom column in postgis
                # (i.e. geometry(MultiPolygon, 25832)
                GrukosAux.msg_box('Lag: "{}" med navn: "{}"  er ikke valid!'.format(tablename, table))
                return

        if table not in ['Styringsværktøj', 'Fremdrift', 'Indmeldinger']:
            # vlayer.getStyleFromDatabase(styleId) or vlayer.loadNamedStyle
            self.add_listlayer(['list_di', 'list_io', 'list_lerlagtilhoer', 'list_potentialetype', 'list_saarbarhed',
                                'list_stamperiode', 'list_status'])

            # TODO These might be removed when qml is loaded from code
            # self.set_date_widget(vlayer)
            # self.set_list_widget(vlayer)
            # self.set_hitten_fields(vlayer)

        # Add layer to map TOC tree root and not current group
        QgsProject.instance().addMapLayer(vlayer, True)

        style_qml = db.get_styleqml(tablename)
        if style_qml:
            #GrukosAux.msg_box(f'style_qml: {style_qml}')
            document = QDomDocument()
            document.setContent(str(style_qml))
            vlayer.importNamedStyle(document)
            GrukosAux.log_info(f'Style applied to guilayer: {table} (=DB layer: {tablename})')
        else:
            GrukosAux.log_info(f'UI layer {table} with DB name {tablename} has no default style in DB')

        # No errors or user trouble - return true
        return True, gkoid

    def add_listlayer(self, tablelist):
        """
        Add all list layer to a group called lists
        http://www.lutraconsulting.co.uk/blog/2014/07/06/qgis-layer-tree-api-part-1/
        http://www.lutraconsulting.co.uk/blog/2014/07/25/qgis-layer-tree-api-part-2/
        """
        db = GrukosDb()
        params = db.get_dbparams()

        root = QgsProject.instance().layerTreeRoot()

        for child in root.children():
            if isinstance(child, QgsLayerTreeGroup):
                # print "- group: " + child.name()
                if child.name() == "lists":  # to check subgroups within test group
                    return

        listGroup = root.addGroup("lists")
        # listGroup = root.insertGroup(5, "lists")  # Add group at index 5 in TOC

        # Prepare connection to database
        uri = QgsDataSourceUri()
        uri.setConnection(params['host'], str(params['port']), params['dbname'], params['user'],
                          params['password'])  # set host name, port, database name, username and password

        for tablename in tablelist:
            uri.setDataSource("gvkort", tablename, None)
            lyr = QgsVectorLayer(uri.uri(), tablename, "postgres")
            if not lyr.isValid():
                # Invalid layer is often a missing integer primary key
                GrukosAux.msg_box('Listetabel: "{}" er ikke valid!'.format(tablename))
                return

            # QgsMapLayerRegistry.instance().addMapLayer(lyr, False)  # False do not add layer free
            QgsProject.instance().addMapLayer(lyr, False)

            node_list = listGroup.addLayer(lyr)

        listGroup.setExpanded(False)

    ### Below code is SUPPOSED TO BE depricated. The style is not loaded through public.layer_styles table
    def set_hitten_fields(self, vlayer):
        fieldIndex = vlayer.fieldNameIndex('kortid')
        vlayer.setEditorWidgetV2(fieldIndex, 'Hidden')
        fieldIndex = vlayer.fieldNameIndex('projektid')
        vlayer.setEditorWidgetV2(fieldIndex, 'Hidden')

    def set_list_widget(self, vlayer):
        def setup_widget(vlayer, list_id_field, list_tabname, list_value_column):
            # Build like:
            # fieldIndex = vlayer.fieldNameIndex('list_io_id')
            # layer_id = QgsMapLayerRegistry.instance().mapLayersByName("list_io")[0].id().encode('cp1252')
            # vlayer.setEditorWidgetV2(fieldIndex, 'ValueRelation')
            # vlayer.setEditorWidgetV2Config(fieldIndex, {'Layer': '{}'.format(layer_id), 'Key': 'list_io_id', 'Value': 'navn', 'allow_null': False})

            fieldIndex = vlayer.fieldNameIndex(list_id_field)
            # layer_id = QgsMapLayerRegistry.instance().mapLayersByName(list_tabname)[0].id().encode('cp1252')
            layer_id = QgsProject.instance().mapLayersByName(list_tabname)[0].id().encode('cp1252')
            vlayer.setEditorWidgetV2(fieldIndex, 'ValueRelation')
            vlayer.setEditorWidgetV2Config(fieldIndex, {'Layer': '{}'.format(layer_id), 'Key': list_id_field,
                                                        'Value': list_value_column, 'allow_null': False})

        def word_counter(fullname, partnamelist):
            for partname in partnamelist:
                if fullname.count(partname) > 0:
                    return True

        layerName = vlayer.name()

        # Set LIST_DI relation widget
        if layerName in [u'Drikkevandsinteresser', u'Følsommeindvindingsområder', u'Indsatsområde'] \
                or word_counter(layerName, ['di (', 'fi (', 'io (']):
            setup_widget(vlayer, 'list_di_id', 'list_di', 'navn')

        # Set LIST_IO relation widget
        if layerName in [u'Følsommeindvindingsområder', u'Indsatsområde'] \
                or layerName.count('fi (') > 0 or layerName.count('io (') > 0:
            setup_widget(vlayer, 'list_io_id', 'list_io', 'navn')

        # Set LIST_LERLAGTILHOER relation widget
        if layerName in [u'Lertykkelse [pktgrid]', u'Lertykkelse [poly]', u'Reduceret lertykkelse [pktgrid]',
                         u'Reduceret lertykkelse [poly]'] \
                or word_counter(layerName,
                                ['lertykkelse_pktgrid (', 'lertykkelse_poly (', 'reduceret_lertykkelse_pktgrid (',
                                 'reduceret_lertykkelse_poly (']):
            setup_widget(vlayer, 'list_lerlagtilhoer_id', 'list_lerlagtilhoer', 'navn')

        # Set LIST_MODELOMR relation widget
        if layerName == u'Modelområder' or layerName.count('modelomr (') > 0:
            # Style: #vlayer.loadNamedStyle('modelomr')
            setup_widget(vlayer, 'list_modelomr_id', 'list_modelomr', 'navn')

        # Set LIST_POTENTIALETYPE relation widget
        if layerName in [u'Potentialekort [pkt]', u'Potentialekort [lin]', u'Potentialekort [poly]'] \
                or word_counter(layerName, ['potentiale_lin (', 'potentiale_pkt (', 'potentiale_poly (']):
            setup_widget(vlayer, 'list_potentialetype_id', 'list_potentialetype', 'type')

        # Set LIST_SAARBARHED relation widget
        if layerName == u'Nitratsårbarhed' or layerName.count('saarbarhed_nitrat (') > 0:
            setup_widget(vlayer, 'list_saarbarhed_id', 'list_saarbarhed', 'saarbarhed')

        # Set LIST_STAMPERIODE relation widget
        if layerName in [u'Indsatsområde', u'Drikkevandsinteresser', u'Følsommeindvindingsområder',
                         u'Indvindingsoplande'] \
                or word_counter(layerName, ['io (', 'di (', 'fi (', 'iol (']):
            setup_widget(vlayer, 'list_stamperiode_id', 'list_stamperiode', 'stamperiode')

        # Set LIST_STATUS relation widget  hertil ......ca...
        if layerName in [u'Modelområder', u'Indsatsområde', u'Indvindingsoplande', u'Drikkevandsinteresser',
                         u'Følsommeindvindingsområder', u'Nitratsårbarhed', u'Redoxgrænsen [pkt]',
                         u'Redoxgrænsen [poly]'] \
                or word_counter(layerName, ['modelomr (', 'io (', 'iol (', 'di (', 'fi (', 'saarbarhed_nitrat (',
                                            'saarbarhed_redox__pkt (', 'saarbarhed_redox__poly (']):
            setup_widget(vlayer, 'list_status_id', 'list_status', 'status')

    def set_date_widget(self, vlayer):
        # Set the date widget on date fields types
        #:param vlayer: Layer to set date widget
        #:return: None

        fields = vlayer.dataProvider().fields()
        for field in fields:
            # print 'field.typeName(): ', field.typeName()
            # print 'field.name(): ', field.name()
            if field.typeName() == 'date':
                # print 'Field type Date: ', field.name()
                fieldIndex = vlayer.fieldNameIndex(field.name())
                vlayer.setEditorWidgetV2(fieldIndex, 'DateTime')
                vlayer.setEditorWidgetV2Config(fieldIndex, {'display_format': 'yyyy-MM-dd', 'allow_null': False,
                                                            'field_format': 'yyyy-MM-dd', 'calendar_popup': True})

    def is_layer_open(self, layername):
        """ Check if a layer already is open i qgis toc
        :param layername: Layername to test if open i map.
        :return: True if layer already is present i map.
        """
        root = QgsProject.instance().layerTreeRoot()

        for child in root.children():
            if isinstance(child, QgsLayerTreeLayer):
                if child.name() == layername:
                    return True
            else:
                if child == layername:
                    return True

        return False

    def zoom_to_gkoids(self, gkoids, table):
        """ Update selection and zoom in map based on array gkoids
        :param gkoids: array of gkoid to zoom to.
        :param table: Table with gkoids to zoom to.
        :return: None

        TODO handle the qgis python warnings on depricated pyqgis methods
        """

        # layer = QgsMapLayerRegistry.instance().mapLayersByName(table)[0]
        layer = QgsProject.instance().mapLayersByName(table)[0]
        if layer is None:
            msg = u'SVANA error in grukos_qgis.zoom_to_gkoids.\nKan ikke finde lag {}'.format(table)
            GrukosAux.msg_box(msg)
            return

        # Select gkoids from layer
        # TODO selection only use for zooming, find a way to zoom without selection
        layer.selectByIds([])  # Clear any selections
        QgsFeatureRequest.NoGeometry = True  # Speed up performance skipping geometry in query
        comma_list_str = ','.join([str(s) for s in gkoids])  # Convert array to comma separated string
        search_value = "({})".format(comma_list_str)  # Add parentheses
        exp = QgsExpression('"gkoid" IN {}'.format(search_value))  # Build final expression

        if exp.hasParserError():
            GrukosAux.log_error(exp.parserErrorString())
        #exp.prepare(layer.fields())  # Use when querying multiple values

        request = QgsFeatureRequest(exp)

        # Get iterator & set selection of layer
        it = layer.getFeatures(request)
        ids = [ft.id() for ft in it]
        if len(ids) == 0:
            GrukosAux.msg_box(
                f'SVANA error in grukos_qggis.zoom_to_gkoids:\n'
                f'Der er ingen poster fundet i tema "{layer.name()}"')
            return

        layer.selectByIds(ids)

        # Zoom to selection
        box = layer.boundingBoxOfSelected()
        self.iface.mapCanvas().setExtent(box)
        layer.selectByIds([])  # Clear any selections
        self.iface.mapCanvas().refresh()

    def select_plant(self, table, searchstr):
        """
        If layer is iol_alle test to see if search matches either anl_id or anl_navn,
        for selection of only searched plant instead of just showing entire iol matvhing gkoid.
        For historical reasons anl_id is a string in the database.
        """
        layer = self.iface.activeLayer()
        if layer is None:
            msg = u'MST error in grukos_qgis.select_plant.\nKan ikke finde lag {}'.format(table)
            GrukosAux.msg_box(msg)
            return False

        if searchstr.isdigit():
            exp = QgsExpression(f'"anl_id" = (\'{searchstr}\')')  # Build final expression
        else:
            exp = QgsExpression(f'"anl_navn" ILIKE (\'{searchstr}\')')  # Build final expression

        if exp.hasParserError():
            GrukosAux.msg_box(f'MST expression parse error: {exp.parserErrorString()}')
            return False

        request = QgsFeatureRequest(exp)

        # Get iterator & set selection of layer
        it = layer.getFeatures(request)
        ids = [ft.id() for ft in it]
        if len(ids) == 0:
            GrukosAux.msg_box(f'MST no records found in "{layer.name()}"')
            return False

        layer.selectByIds(ids)

        return True
