# -*- coding: utf-8 -*-
from __future__ import absolute_import
import os.path
from builtins import object

from qgis.PyQt.QtCore import QSettings, QTranslator, qVersion, QCoreApplication, Qt
from qgis.PyQt.QtWidgets import QAction
from qgis.PyQt.QtGui import QIcon

from .grukos_dockwidget import GrukosDockWidget

# Initialize Qt resources from file resources.py
# DO NOT OPTIMIZE IMPORT since ressources are necessary import not detected by pycvharm
from . import resources


class Grukos(object):
    """QGIS Plugin Implementation."""

    def __init__(self, iface):
        """Constructor.

        :param iface: An interface instance that will be passed to this class
            which provides the hook by which you can manipulate the QGIS
            application at run time.
        :type iface: QgsInterface
        """

        # Save reference to the QGIS interface
        self.iface = iface

        # initialize plugin directory
        self.plugin_dir = os.path.dirname(__file__)

        # initialize locale
        locale = QSettings().value('locale/userLocale')[0:2]
        locale_path = os.path.join(
            self.plugin_dir,
            'i18n',
            'Grukos_{}.qm'.format(locale))

        if os.path.exists(locale_path):
            self.translator = QTranslator()
            self.translator.load(locale_path)

            if qVersion() > '4.3.3':
                QCoreApplication.installTranslator(self.translator)

        # Declare instance attributes
        self.actions = []
        self.menu = self.tr(u'GRUKOS')

        # TODO: We are going to let the user set this up in a future iteration

        # self.toolbar = self.iface.addToolBar(u'GRUKOS toolbar')
        # self.toolbar.setObjectName(u'GRUKOS')

        self.pluginIsActive = False
        self.dockwidget = None

        # GrukosAux.startdebugging()

        #import sys
        #sys.path.append(r'C:\Program Files\JetBrains\PyCharm 2019.2.3\debug-eggs\pydevd-pycharm.egg')
        #import pydevd
        #from pydev import pydevd

        #pip install pydevd-pycharm~= 192.6817.19
        #import pydevd_pycharm
        #pydevd_pycharm.settrace('localhost', port=53100, stdoutToServer=True, stderrToServer=True)

        #import pydevd_pycharm
        #pydevd_pycharm.settrace('localhost', port=$SERVER_PORT, stdoutToServer = True, stderrToServer = True)

    # noinspection PyMethodMayBeStatic
    def tr(self, message):
        """Get the translation for a string using Qt translation API.

        We implement this ourselves since we do not inherit QObject.

        :param message: String for translation.
        :type message: str, QString

        :returns: Translated version of message.
        :rtype: QString
        """
        # noinspection PyTypeChecker,PyArgumentList,PyCallByClass
        return QCoreApplication.translate('Grukos', message)


    def add_action(
        self,
        icon_path,
        text,
        callback,
        enabled_flag=True,
        add_to_menu=True,
        add_to_toolbar=True,
        status_tip=None,
        whats_this=None,
        parent=None):
        """Add a toolbar icon to the toolbar.

        :param icon_path: Path to the icon for this action. Can be a resource
            path (e.g. ':/plugins/foo/bar.png') or a normal file system path.
        :type icon_path: str

        :param text: Text that should be shown in menu items for this action.
        :type text: str

        :param callback: Function to be called when the action is triggered.
        :type callback: function

        :param enabled_flag: A flag indicating if the action should be enabled
            by default. Defaults to True.
        :type enabled_flag: bool

        :param add_to_menu: Flag indicating whether the action should also
            be added to the menu. Defaults to True.
        :type add_to_menu: bool

        :param add_to_toolbar: Flag indicating whether the action should also
            be added to the toolbar. Defaults to True.
        :type add_to_toolbar: bool

        :param status_tip: Optional text to show in a popup when mouse pointer
            hovers over the action.
        :type status_tip: str

        :param parent: Parent widget for the new action. Defaults None.
        :type parent: QWidget

        :param whats_this: Optional text to show in the status bar when the
            mouse pointer hovers over the action.

        :returns: The action that was created. Note that the action is also
            added to self.actions list.
        :rtype: QAction
        """

        icon = QIcon(icon_path)
        action = QAction(icon, text, parent)
        action.triggered.connect(callback)
        action.setEnabled(enabled_flag)

        if status_tip is not None:
            action.setStatusTip(status_tip)

        if whats_this is not None:
            action.setWhatsThis(whats_this)

        if add_to_toolbar:
            #self.toolbar.addAction(action)    # creates a new toolbar and adds the tool
            self.iface.addToolBarIcon(action)  # adds tool to plugin toolbar

        if add_to_menu:
            self.iface.addPluginToMenu(
                self.menu,
                action)

        self.actions.append(action)

        return action


    def initGui(self):
        """Create the menu entries and toolbar icons inside the QGIS GUI."""

        #print "***INITGUI..."
        icon_path = ':/plugins/grukos/icons/grukos.png'
        self.add_action(
            icon_path,
            text=self.tr(u'GRUKOS'),
            callback=self.run,
            status_tip=u'Start GRUKOS',
            parent=self.iface.mainWindow())

        # Menu items - manual way of add_action
        self.aboutAction = QAction( QIcon(), 'Om...', self.iface.mainWindow() )
        self.aboutAction.triggered.connect(self.show_about_dlg)
        self.iface.addPluginToMenu("GRUKOS", self.aboutAction)

    def show_about_dlg(self):
        # print 'show_about_dlg...'
        from .grukos_about_dlg import GrukosAboutDlg
        GrukosAboutDlg(self.iface.mainWindow()).exec_()

    def onClosePlugin(self):
        """Cleanup necessary items here when plugin dockwidget is closed"""
        # disconnects
        self.dockwidget.closingPlugin.disconnect(self.onClosePlugin)

        # remove this statement if dockwidget is to remain for reuse if plugin is reopened
        # Commented next statement since it causes QGIS crashe when closing the docked window:
        # self.dockwidget = None

        self.pluginIsActive = False

    def unload(self):
        """Removes the plugin menu item and icon from QGIS GUI."""
        for action in self.actions:
            self.iface.removePluginMenu(self.tr(u'GRUKOS'), action)

        # TODO why does these two action not removed from menu in above code ?
        self.iface.removePluginMenu(self.tr(u'GRUKOS'), self.aboutAction)

        self.iface.removeToolBarIcon(action)

        # remove the toolbar
        # del self.toolbar

    def run(self):
        """Run method that loads and starts the plugin"""

        if not self.pluginIsActive:
            self.pluginIsActive = True

            # dockwidget may not exist if:
            #    first run of plugin
            #    removed on close (see self.onClosePlugin method)
            if self.dockwidget == None:
                # Create the dockwidget (after translation) and keep reference
                self.dockwidget = GrukosDockWidget(self.iface)

            # connect to provide cleanup on closing of dockwidget
            self.dockwidget.closingPlugin.connect(self.onClosePlugin)

            # show the dockwidget
            # TODO: fix to allow choice of dock location
            self.iface.addDockWidget(Qt.RightDockWidgetArea, self.dockwidget)
            self.dockwidget.show()
