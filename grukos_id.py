# -*- coding: utf-8 -*-
"""
/**************************************************************************************
 Grukos user id input handler.
 The user can input a comma separated string of mixed gkoid (int) and kortnavn (string)

Jakob Lanstorp, Miljøstyrelsen, januar 2017
**************************************************************************************/
"""
from __future__ import absolute_import

from builtins import str
from builtins import object
from .grukos_db import *

"""
Sample input string         : "320511, langeland, 320553"
Sample output dictionary    : [[320511, 'Engbjerg-Tolum'], [320555, 'Langeland'], [320553, 'Nordfyn']]
"""

class GrukosId(object):
    """
    Validates gkoid to kortnavn and viceversa also for wildcard character %.
    Only returning valid items and skipping the rest.
    Returning an array of tuples:  [(320391, 'Nibe - Aalborg'), (320394, 'Aalborg sydvest - Aalborg')]
    """
    def load(self, id_str):

        # Strip and split input
        id_str = str(id_str)  # might be an integer
        gids = id_str.strip().split(',')

        # Strip id element in array
        gids = [gid.strip() for gid in gids]

        gids_result = []
        db = GrukosDb()
        for gid in gids:
            gid = gid.strip()
            if gid.isdigit():
                # Assume input is a gkoid
                gkoid = int(gid)
                kortnavn = db.gko_gkoid_to_kortnavn(gkoid)
                if kortnavn:
                    gids_result.append((gkoid, kortnavn))
            else:
                # Assume input is a kortlægningsnavn
                kortnavn = gid
                # Check for wildcard % search
                if '%' in kortnavn:
                    wildcard_result = db.gko_ilike_kortnavn_to_gkoid(kortnavn)
                    if wildcard_result:
                        for key, value in wildcard_result.items():
                            gkoid = key
                            kortnavn2 = value
                            if kortnavn2:
                                gids_result.append((gkoid, kortnavn2))
                else:
                    gkoid = db.gko_kortnavn_to_gkoid(kortnavn)
                    if gkoid and type(gkoid) == int:
                        gids_result.append((gkoid, kortnavn))

        if len(gids_result) == 0:
            return None
        else:
            return gids_result


class TestGrukosId(object):

    #teststr = '320555, langeland, 320553'
    #teststr = ''
    teststr = '%borg%'

    gid = GrukosId()
    test = gid.load(teststr)

    #print test


# Run Test
#test = TestGrukosId()









