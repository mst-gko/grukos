# -*- coding: utf-8 -*-
"""
***************************************************************************
 Grukos general auxillary methods
***************************************************************************
"""
# from future import standard_library
# standard_library.install_aliases()
from builtins import str
from builtins import object
from qgis.core import QgsMessageLog, QgsApplication
from qgis.PyQt.QtWidgets import QMessageBox
from qgis.gui import QgsMessageBar

import configparser
import os

import sys
# reload(sys)
# sys.setdefaultencoding('utf8')

class GrukosAux(object):
    GRUKOS = 'GRUKOS'

    @staticmethod
    def startdebugging():
        #sys.path.append(r'C:\Program Files (x86)\JetBrains\PyCharm 2016.3.2\debug-eggs\pycharm-debug.egg')
        #from pydev import pydevd
        #import pydevd
        #pydevd.settrace('localhost', port=53100, stdoutToServer=True, stderrToServer=True)
        pass

    @staticmethod
    def get_meta_name():
        config = configparser.ConfigParser()
        config.read(os.path.join(os.path.dirname(__file__),'metadata.txt'))

        name = str(config.get('general', 'name'))
        #GrukosAux.log_info('{}'.format(name))

        return name

    @staticmethod
    def get_meta_version():
        config = configparser.ConfigParser()
        config.read(os.path.join(os.path.dirname(__file__), 'metadata.txt'))

        version = str(config.get('general', 'version'))
        return version

    @staticmethod
    def messagebar_info(iface, msg, title='Info'):
        """ pop a msg  - SUCCESS, INFO, WARNING and CRITICAL"""
        iface.messageBar().pushMessage(title, msg, level=QgsMessageBar.INFO,  duration=3)

    @staticmethod
    def statuabar_info(iface, msg):
        """ Write a msg to statusbar"""
        iface.showMessage(msg)

    @staticmethod
    def log_info(msg):
        """ Write to qgis log windows """
        str_msg = str(msg)
        QgsMessageLog.logMessage(str_msg, GrukosAux.GRUKOS, 0)

    @staticmethod
    def log_warning(msg):
        str_msg = str(msg)
        QgsMessageLog.logMessage(str_msg, GrukosAux.GRUKOS, 1)

    @staticmethod
    def log_error(msg):
        str_msg = str(msg)
        QgsMessageLog.logMessage(str_msg, GrukosAux.GRUKOS, 2)

    @staticmethod
    def msg_box(msg):
        QMessageBox.information(None, GrukosAux.GRUKOS, "{}".format(msg),
                                QMessageBox.Ok)

    @staticmethod
    def msg_box_yes_no(msg):
        reply = QMessageBox.question(None, 'Vælg', "{}".format(msg),
                                     QMessageBox.No | QMessageBox.Yes)
        if reply == QMessageBox.Yes:
            return True
        else:
            return False


    @staticmethod
    def enable_qgis_log(filename='D:\qgis.log', haltApp = False, haltMsg = 'stop'):
        """ Very useful when QGIS crashes on PGSQL error
        :param filename: Filename and path for log file
        :param haltApp: Halts the application with a modal dialog
        :param haltMsg: Message to user when showing model stopping dialog
        :rtype: None
        """
        def write_log_message(message, tag, level):
            with open(filename, 'a') as logfile:
                logfile.write('{tag}({level}): {message}'.format(tag=tag, level=level, message=message))

        QgsApplication.messageLog().messageReceived.connect(write_log_message)

        if haltApp:
            QMessageBox.information(None, GrukosAux.GRUKOS, "{}".format(haltMsg), QMessageBox.Ok)

