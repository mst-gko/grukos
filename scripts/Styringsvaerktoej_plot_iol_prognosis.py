# Definition of inputs and outputs
##GRUKOS=group
##iol_prognosis=output file png

from qgis.core import *
import numpy as np
from datetime import datetime
import calendar
import matplotlib.pyplot as plt
import matplotlib.dates
import csv

def datestring(arg1, arg2=None, arg3=None):
    if arg2 and arg3:
        return '{}-{:02}-{:02}'.format(arg1, arg2, arg3)
    else:
        return '{}-{:02}-{:02}'.format(arg1.year, arg1.month, arg1.day)

# Read data from styringsvaerktoej
styringsvaerktoej = processing.getObject('styringsvaerktoej')

if styringsvaerktoej is None:
    raise Exception('Could not open "styringsvaerktoej" layer. Please load it beforehand.')

forv_afsl = []
dato_afsl = []
prioritering_kat = []
for f in styringsvaerktoej.getFeatures():
    forv_afsl.append(datestring(f['forv_afsl'].toPyDateTime()))
    dato_afsl.append(datestring(f['dato_afsluttet'].toPyDateTime()))
    prioritering_kat.append(f['prioritering_kat'])
    
#QgsMessageLog.logMessage(str(forv_afsl))
#QgsMessageLog.logMessage(str(dato_afsl))

forv_afsl = np.asarray(forv_afsl, dtype='datetime64[D]')
dato_afsl = np.asarray(dato_afsl, dtype='datetime64[D]')
prioritering_kat = np.asarray(prioritering_kat, dtype=np.unicode_)
QgsMessageLog.logMessage('Read ' + str(len(forv_afsl)) + ' values...')
included_in_sum = np.zeros_like(forv_afsl, dtype=np.int)

# Bin projects according to month

# Specify time manually for back-projections
# month_current = 13  # use 13 to get end of year status of date_afsl
# year_current = 2018

# Use current time
month_current = datetime.now().month
year_current = datetime.now().year

t = np.empty(13, dtype='datetime64[D]')
t[0] = datestring(year_current, 1, 1)
for i in np.arange(1, 12+1):
    t[i] = datestring(year_current, i, calendar.monthrange(year_current, i)[1])

n_iol = np.zeros(12+1)
# finished IOLs
#for i in np.arange(1, month_current+1):
for i in np.arange(1, 12+1):
    if i > 0:
        n_iol[i] = n_iol[i-1]

    for j in np.arange(len(dato_afsl)):

        if dato_afsl[j] >= t[0] and \
           dato_afsl[j] <= t[i] and \
           (prioritering_kat[j] == 'Skal-opgave' or \
            prioritering_kat[j] == 'Synergi m. skal-opgave' or \
            prioritering_kat[j] == 'Direkte rettelser') and \
           included_in_sum[j] == 0:

            included_in_sum[j] = 1
            n_iol[i] += 1

# prognosis
for i in np.arange(month_current, 12+1):
    if i > 0:
        n_iol[i] = n_iol[i-1]

    for j in np.arange(len(forv_afsl)):

        if (forv_afsl[j] >= t[0] and \
            forv_afsl[j] <= t[i] and \
            (prioritering_kat[j] == 'Skal-opgave' or \
             prioritering_kat[j] == 'Synergi m. skal-opgave' or \
             prioritering_kat[j] == 'Direkte rettelser')) and \
           included_in_sum[j] == 0:

            included_in_sum[j] = 1
            n_iol[i] += 1

#QgsMessageLog.logMessage(str(t))
#QgsMessageLog.logMessage(str(n_iol))

# Plot yearly prognosis
prog_t = []
prog_iol = []
with open('F:/GKO/admin/ressourcer/fremdrift/2019/iol_prognosis.csv',  'rb') as f:
    for line in f.readlines():
        content = line.split(',')
        prog_t.append(datetime.strptime(content[0], '%Y-%m-%d'))
        prog_iol.append(int(content[1]))

QgsMessageLog.logMessage(str(prog_t))
QgsMessageLog.logMessage(str(prog_iol))

# Plot results
fig, ax = plt.subplots()
fig.set_figheight(3)
fig.set_figwidth(6)
plt.plot_date(prog_t,
              prog_iol,
              ':', color='red', lw=3,
              label='Indvindingsoplande prognose (oprindelig)')
plt.plot_date(t.astype(datetime)[0:month_current],
              n_iol[0:month_current],
              '-', color='black', lw=3,
              label='Indvindingsoplande')
plt.plot_date(t.astype(datetime)[month_current-1:],
              n_iol[month_current-1:],
              '-', color='gray', lw=3,
              label='Indvindingsoplande prognose')

plt.legend()

# Configure plot styling
ax.set_xlim([datetime(year_current, 1, 1),
                   datetime(year_current, 12, 31)])
ax.set_title(str(year_current))
ax.xaxis.set_major_locator(matplotlib.dates.YearLocator())
ax.xaxis.set_minor_locator(matplotlib.dates.MonthLocator(range(1,12+1)))
#ax.xaxis.set_major_formatter(matplotlib.dates.DateFormatter('\n%Y'))
ax.xaxis.set_major_formatter(matplotlib.dates.DateFormatter(''))
ax.xaxis.set_minor_formatter(matplotlib.dates.DateFormatter('%b'))
plt.setp(ax.get_xticklabels(), rotation=0, ha='center')
ax.tick_params(axis='x', which='both', length=4)
fig.tight_layout()

# Save plot to output file
plt.savefig(iol_prognosis, dpi=600, transparent=True)
QgsMessageLog.logMessage('Saved plot to ' + iol_prognosis)
