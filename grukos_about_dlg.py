# -*- coding: utf-8 -*-

from __future__ import absolute_import
import platform
from .grukos_db import *
from PyQt5 import QtGui, QtWidgets, uic

try:
    from . import resources
except ImportError:
    import resources_rc


class GrukosAboutDlg(QtWidgets.QDialog):

    def __init__(self, parent=None):
        super().__init__()

        self.ui = uic.loadUi(os.path.join(os.path.dirname(__file__), 'grukos_about.ui'), self)
        self.logo.setPixmap(QtGui.QPixmap(":/plugins/grukos/icons/mst_logo.png"))

        body = f"""
        <span>
            {str(GrukosAux.get_meta_name())} version <b>{GrukosAux.get_meta_version()}</b>
            <br>You are logged in as <b>{GrukosDb.get_userrole()}</b>
            <br>Python version: {platform.python_version()}
            <br>Platform: {platform.system()} - {platform.version()}
            <br>
            <br>Dette plugin er udviklet og vedligeholdt af Miljøstyrelsens afdeling for grundvandskortlægning.
            <br>GRUKOS indeholder data fra Miljøstyrelsen tidligere, nuværende og fremtidige kortlægningsprojekter.
            <br>
            <br><a href="https://mst.dk/gko">Miljøstyrelsens Grundvandskortlægning</a>
            <br><a href="https://mst.dk/natur-vand/vand-i-hverdagen/grundvand/grundvandskortlaegning/kortlaegning-2015/">Miljøstyrelsens grundvandsrapporter 2015</a>
            <br><a href="https://mst.dk/natur-vand/vand-i-hverdagen/grundvand/grundvandskortlaegning/kortlaegning-2016-2020/afrapportering-2016-2020/">Miljøstyrelsens grundvandsrapporter 2016-2020</a>
            <br>
            <br>Developer jakla, simak, andam
        </span>
        """

        self.txt.setHtml(body)

        #TODO make the email template
        ##mail = QtCore.QUrl("mailto:abc@abc.com")
        #mail.addQueryItem("subject", subject)
        #mail.setQuery("subject", subject)
        #mail.setQuery("body", body)

        #text = text.replace("$MAIL_SUBJECT$", str(mail.encodedQueryItemValue("subject")))
        #text = text.replace("$MAIL_BODY$", str(mail.encodedQueryItemValue("body")))

        #text = text.replace("$MAIL_SUBJECT$", str(mail["subject"]))
        #text = text.replace("$MAIL_BODY$", str(mail.encodedQueryItemValue("body")))


