from qgis.core import QgsApplication
from qgis.PyQt.QtWidgets import QDialog

GUIEnabled=True
app = QgsApplication([], GUIEnabled)

dlg = QDialog()
dlg.exec_()

app.exit(app.exec_())