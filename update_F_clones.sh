#!/usr/bin/env bash
set -e

function update_to_latest_tag {
    echo "cd $1"
    cd "$1" || exit 1
    git fetch --tags
    latesttag="$(git describe --abbrev=0 --tags)"
    echo "Checking out $latesttag"
    git checkout "$latesttag"
}

update_to_latest_tag "$HOME/f/GKO/data/grukos/grukos/plugin/grukos-qgis2"
update_to_latest_tag "$HOME/f/GKO/data/grukos/grukos/plugin/grukos-qgis3"
