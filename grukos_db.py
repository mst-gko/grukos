# -*- coding: utf-8 -*-
"""
    Grukos db access layer class
    Jakob Lanstorp, SVANA 19-09-2016

    NOTE:
        -QGIS crashes hard on any db error
        -It is GrukosDb responsibility to quote strings
        -Always encapsulate sql in double quotes "", since single quotes '' are reserved for add string params to sql
"""
from __future__ import print_function
from __future__ import absolute_import

from builtins import object
import psycopg2
import psycopg2.extras
from .grukos_aux import *
import base64
import json


class GrukosUser(object):
    LOCALHOST = 'localhost'
    READER = 'grukosreader'
    ADMIN = 'grukosadmin'


""" GrucosDb wrapper class to DB queries """


class GrukosDb(object):

    def __init__(self):
        pass

    def test_connection(self):
        """ Test database connection """
        conn = None
        try:
            conn = psycopg2.connect(**self.get_dbparams())
            conn.close()
            return True
        except:
            return False
        finally:
            if conn:
                conn.close()

    def get_version(self):
        """ Get version of PostgreSQL"""
        sql = 'SELECT Version()'

        cur = self.execute_sql(sql, dict_cursor=False)
        row_tuple = cur.fetchone()
        ver = row_tuple[0]
        return ver

    def get_connection(self):
        return psycopg2.connect(**self.get_dbparams())

    """
        TODO: Implement all function in this module as below
    """
    def is_admin(self, username: str) -> bool:
        connection = self.get_connection()

        with connection.cursor() as cursor:
            cursor.execute("""
                SELECT
                    admin
                FROM
                    users
                WHERE
                    username = %(username)s
            """, {
                'username': username
            })
            result = cursor.fetchone()

        if result is None:
            # User does not exist
            return False

        admin, = result
        return admin

    def execute_sql(self, sql, dict_cursor=True, print_sql=False):

        """ Execute a SQL query
        :param sql: SQL to be executed
        :param dict_cursor: Flag indicating if cursor is a dict or not. Use false for scalar queries
        :param print_sql: Flag indicating if sql is to be printet
        :return: returns a cursor
        """

        if print_sql:  # fix_print_with_import
            print(sql)
        conn = psycopg2.connect(**self.get_dbparams())

        if dict_cursor:
            cur = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
        else:
            cur = conn.cursor()

        try:
            cur.execute(sql)
            return cur
        except psycopg2.DatabaseError as e:
            # fix_print_with_import
            print('grukos SQL error: {}'.format(e))
            GrukosAux.log_error('grukos SQL error: {}'.format(e))
            # sys.exit(1)
        finally:
            pass
            # TODO
            # if conn:
            #    conn.close()

    def mogrify(self, sql, dict_cursor=True):
        """ Test how psycopg2 renderes the sql before sending it to the database a SQL query
        :param sql: SQL to be testet
        :param dict_cursor: Flag indicating if cursor is a dict or not. Use false for scalar queries
        :return: returns checked sql string
        """

        conn = psycopg2.connect(**self.get_dbparams())

        if dict_cursor:
            cur = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
        else:
            cur = conn.cursor()

        try:
            check_sql = cur.mogrify(sql)
            return check_sql
        except psycopg2.DatabaseError as e:
            # fix_print_with_import
            print('SVANA Error {}'.format(e))
            GrukosAux.log_error('SVANA Error {}'.format(e))
            sys.exit(1)
        finally:
            pass
            if conn:
                conn.close()

    def count_afg(self, gkoid):
        """ Count records matching gkoid in table afg """

        try:
            sql = 'SELECT Count(*) AS count FROM gvkort.afg WHERE gkoid = {};'.format(gkoid)

            cur = self.execute_sql(sql)
            rows_tuple = cur.fetchone()
            count = rows_tuple[0]

            return count, sql
        except Exception as e:
            # fix_print_with_import
            print('SVANA Error {}'.format(e))
            GrukosAux.log_error('SVANA Error {}'.format(e))
            sys.exit(1)
        finally:
            pass
            # if self.conn:
            #    self.conn.close()

    def gko_get_gkoid(self, anlaegsno):
        sql = f"SELECT gkoid FROM gvkort.iol_alle WHERE anl_id = '{anlaegsno}';"
        # GrukosAux.enable_qgis_log()
        cur = self.execute_sql(sql)
        rows_tuple = cur.fetchone()

        # GrukosAux.log_info(rows_tuple)
        if rows_tuple == None:
            return '', sql

        gkoid = rows_tuple[0]

        return gkoid, sql

    def gko_get_projektid(self, gkoid):
        """ Use a PostgreSQL function to translate gkoid to projektid """
        # print 'gko_get_projektid(self, gkoid)'
        sql = f'SELECT gvkort.gko_getprojektid({gkoid});'
        cur = self.execute_sql(sql)
        rows_tuple = cur.fetchone()
        projektid = rows_tuple[0]

        return projektid, sql

    """ LEGACY CODE - OBSOLUTE
    def gko_get_modelomrid(self, navn):
        # Use a PostgreSQL function to translate modelomrnavn to id
        #print 'gko_get_modelomrid(self, modelomrnavn)'
        sql = "SELECT gvkort.gko_get_modelomrid('{}');".format(navn)
        cur = self.execute_sql(sql)
        rows_tuple = cur.fetchone()
        id = rows_tuple[0]

        return id, sql
    """

    def gko_fullsearch_db(self, gkoid=0, kortnavn=''):
        """ Get a list of tables that have a matching gkoid or kortnavn """
        if gkoid > 0:
            sql = 'SELECT gvkort.gko_fullsearch_db({});'.format(gkoid)
            cur = self.execute_sql(sql)
            rows_tuple = cur.fetchone()
            tables = rows_tuple[0]
            return tables
        elif len(kortnavn) > 0:
            gkoid = self.gko_kortnavn_to_gkoid(kortnavn)
            # print 'gkoid: ', gkoid
            if gkoid:
                tables = self.gko_search_db(gkoid)
                return tables
            else:
                return
        else:
            GrukosAux.log_error('SVANA: invalid input to grukos_db.gko_fullsearch_db')
            raise Exception('SVANA: invalid input to grukos_db.gko_fullsearch_db')

    def gko_partsearch_db(self, db_layers, gkoid=0, kortnavn=''):
        """ Get a list of tables that have a matching gkoid or kortnavn for a given list of tables """
        if gkoid > 0:
            # Demo: SELECT gvkort.gko_partsearch_db( array['io', 'afgpolygon'], 320511 );
            sql = 'SELECT gvkort.gko_partsearch_db(array{},{});'.format(db_layers, gkoid)

            # GrukosAux.log_info(sql)

            cur = self.execute_sql(sql)

            # test_sql = self.mogrify(sql)
            # GrukosAux.msg_box(sql)

            rows_tuple = cur.fetchone()
            tables = rows_tuple[0]
            return tables
        elif len(kortnavn) > 0:
            gkoid = self.gko_kortnavn_to_gkoid(kortnavn.title())

            # GrukosAux.msg_box(kortnavn)
            # GrukosAux.msg_box(str(gkoid))

            # print 'gkoid: ', gkoid
            if gkoid:
                # TODO potential never stopping recursive call
                tables = self.gko_partsearch_db(db_layers, gkoid=gkoid)
                return tables
            else:
                return
        else:
            GrukosAux.log_error('SVANA: invalid input to grukos_db.gko_partsearch_db')
            raise Exception('SVANA: invalid input to grukos_db.gko_partsearch_db')

    def get_styleqml(self, style_name):
        #sql = "SELECT styleqml FROM public.layer_styles WHERE stylename = '{}';".format(style_name)

        sql = f"""
        SELECT styleqml FROM public.layer_styles WHERE stylename = '{style_name}' AND f_table_schema = 'gvkort';
        """

        # GrukosAux.enable_qgis_log(haltApp=True)

        cur = self.execute_sql(sql)
        rows_tuple = cur.fetchone()

        if not rows_tuple:
            return None

        style = rows_tuple[0]

        return style

    def gko_linksearch(self, link_layer, gkoid=0, kortnavn=''):
        # Get a list of records from deeplink table matching gkoid/kortnavn and deeplink type
        try:
            if int(gkoid) > 0:
                GrukosAux.log_info('link_layer: {}'.format(link_layer))

                # link_layers: ['deeplinkgeoscene', 'deeplinkmodeldb', 'deeplinkstyrelse', 'deeplinkrefbib']
                # Tested ok: SELECT link, list_deeplink_id FROM gvkort.deeplink WHERE gkoid=320555 AND list_deeplink_id = any( array[2,1] ) AND link IS NOT NULL;
                sql = 'SELECT gkoid, link FROM {} WHERE gkoid={} AND link IS NOT NULL;'
                if link_layer == 'deeplinkmodeldb':
                    sql = sql.format('gvkort.deeplinkmodeldb', gkoid)
                elif link_layer == 'deeplinkgeoscene':
                    sql = sql.format('gvkort.deeplinkgeoscene', gkoid)
                elif link_layer == 'deeplinkrefbib':
                    sql = sql.format('gvkort.deeplinkrefbib', gkoid)
                elif link_layer == 'deeplinkstyrelse':
                    sql = sql.format('gvkort.deeplinkstyrelse', gkoid)
                else:
                    GrukosAux.log_error('MST: link_types not valid in gko_linksearch')
                    raise Exception('MST: link_types not valid in gko_linksearch')

                # GrukosAux.log_info(sql)

                cur = self.execute_sql(sql)
                rows_tuple = cur.fetchall()

                # TODO A designated list of dataobjects should be return instead, so access is not index based
                # TODO but like array of tuple[0].gkoid, tuple[0].link, tuple[0].list_deeplink_id
                # TODO USE a database ORM framework like PonyORM or
                return rows_tuple  # [ gkoid, link ]

            elif len(kortnavn) > 0:
                for linklayer in link_layer:
                    # fix_print_with_import
                    print(linklayer)
            else:
                GrukosAux.log_error('MST: invalid input to grukos_db.gko_linksearch')
                raise Exception('MST: invalid input to grukos_db.gko_linksearch')
        except Exception as e:
            GrukosAux.log_error('gko_linksearch: {}'.format(e))
            GrukosAux.msg_box('gko_linksearch: {}'.format(e))

    """
        OBSOLETE - KEEPS FOR SQL ARRAY SAMPLE
        def gko_linksearch(self, link_layers, gkoid=0, kortnavn=''):
        # Get a list of records from deeplink table matching gkoid/kortnavn and deeplink type
        try:
            if gkoid > 0:
                GrukosAux.log_info('link_layers: {}'.format(link_layers))
                link_types = []
                for link in link_layers:
                    #GrukosAux.log_info('link: {}'.format(link))

                    # link_layers: ['deeplinkgeoscene', 'deeplinkmodeldb', 'deeplinkstyrelse', 'deeplinkrefbib']

                    #TODO: deeptable has been split to four tables - no type anymore

                    if 'modeldb' in link.lower(): link_types.append(2)
                    elif 'geoscene' in link.lower(): link_types.append(3)
                    elif 'reference' in link.lower(): link_types.append(4)
                    elif 'styrelse' in link.lower(): link_types.append(5)
                    else:
                        GrukosAux.log_error('MST: link_types not valid in gko_linksearch')
                        raise Exception('MST: link_types not valid in gko_linksearch')

                # Tested ok: SELECT link, list_deeplink_id FROM gvkort.deeplink WHERE gkoid=320555 AND list_deeplink_id = any( array[2,1] ) AND link IS NOT NULL;
                sql = 'SELECT gkoid, link, list_deeplink_id FROM gvkort.deeplink WHERE gkoid={} AND list_deeplink_id = ANY( ARRAY{} ) AND link IS NOT NULL;'.format(gkoid, link_types)
                GrukosAux.log_info(sql)

                #GrukosAux.enable_qgis_log()
                #GrukosAux.msg_box('Stop !!!')

                cur = self.execute_sql(sql)
                rows_tuple = cur.fetchall()

                # TODO A designated list of dataobjects should be return instead, so access is not index based
                # TODO but like array of tuple[0].gkoid, tuple[0].link, tuple[0].list_deeplink_id
                # USE a database ORM framework like PonyORM or
                return rows_tuple   # [ [gkoid1, link1, list_deeplink_id1], [gkoid2, link2, list_deeplink_id2] [...]... ]

            elif len(kortnavn) > 0:
                for linklayer in link_layers:
                    print linklayer
            else:
                GrukosAux.log_error('MST: invalid input to grukos_db.gko_linksearch')
                raise Exception('MST: invalid input to grukos_db.gko_linksearch')
        except Exception, e:
            GrukosAux.log_error('gko_linksearch: {}'.format(e))
            GrukosAux.msg_box('gko_linksearch: {}'.format(e))
    """

    def gko_ilike_partsearch_db(self, db_layers, wildcard_kortnavn):
        dict = self.gko_ilike_kortnavn_to_gkoid(wildcard_kortnavn)
        tables_result = {}
        for key_gkoid, value_kortnavn in dict.items():
            GrukosAux.log_info('gko_ilike_partsearch_db - {} : {}'.format(key_gkoid, value_kortnavn))
            tables = self.gko_partsearch_db(db_layers, gkoid=key_gkoid)
            # GrukosAux.log_info('tables: {}'.format(tables))
            if tables:
                # Use kortnavn and not gkoid as key for usesability
                tables_result[value_kortnavn] = tables

        # GrukosAux.log_info('tables_result: {}'.format(tables_result))
        return tables_result

    def gko_ilike_kortnavn_to_gkoid(self, kortnavn):
        """ Get ILIKE search, returns dictionary"""
        # GrukosAux.enable_qgis_log()

        sql = "SELECT gkoid, sagsnavn from gvkort.gkoprojekt where sagsnavn ilike '{}';".format(kortnavn)
        cur = self.execute_sql(sql)
        rows_tuple = cur.fetchall()

        result = {}
        for row in rows_tuple:
            gkoid = row[0]
            kortnavn = row[1]
            result[gkoid] = kortnavn

        return result

    def gko_linkid_to_linktype(self, linkid):
        """ list_deeplink_id to link"""

        # GrukosAux.enable_qgis_log()

        sql = "SELECT linktype from gvkort.list_deeplink where list_deeplink_id = {};".format(linkid)
        cur = self.execute_sql(sql)
        rows_tuple = cur.fetchone()
        link = rows_tuple[0]

        return link

    def gko_bound_fullsearch(self, x1, y1, x2, y2):
        """  Spatial search tables with geom intersection rectangle
        :param x1: x of upper left point
        :param y1: y of upper left point
        :param x2: x of lower right point
        :param y2: y of lower right point
        :return: Array of tables with geom intersecting rectangle
        """
        sql = 'SELECT gvkort.gko_bound_search({},{},{},{})'.format(x1, y1, x2, y2)
        # print sql
        cur = self.execute_sql(sql)
        rows_tuple = cur.fetchone()
        tables = rows_tuple[0]
        return tables

    def gko_bound_partsearch(self, x1, y1, x2, y2, layers):
        """  Spatial search tables with geom intersection rectangle for a given list of layers
        :param x1: x of upper left point
        :param y1: y of upper left point
        :param x2: x of lower right point
        :param y2: y of lower right point
        :param layers: specific list of layers to query
        :return: Array of tables with geom intersecting rectangle
        """
        # Do to the impossiblities to use plpgsql execute for returning multiple rows, this sql is completely written i Python
        result = []
        for layer in layers:
            sql = "SELECT DISTINCT '{}' AS tab, gkoid, kortnavn FROM gvkort.{} WHERE ST_Intersects(geom, ST_MakeEnvelope({},{},{},{},25832));".format(
                layer, layer, x1, y1, x2, y2)
            cur = self.execute_sql(sql)
            rows = cur.fetchall()

            for row in rows:
                gkoid = row[1]
                kortnavn = self.gko_gkoid_to_kortnavn(gkoid)
                result.append(
                    '{} ({} - {})'.format(layer, gkoid, kortnavn))  # XXX Only place where result is formated...

        return result

    def gko_bound_partsearch_deeplink(self, x1, y1, x2, y2, deeplinklayers):
        """  Spatial search tables with geom intersection rectangle for a given list of layers
        :param x1: x of upper left point
        :param y1: y of upper left point
        :param x2: x of lower right point
        :param y2: y of lower right point
        :return: Array of tables with geom intersecting rectangle
        """

        # GrukosAux.enable_qgis_log()

        # Get the gkoids in bound from selected tables
        sql = "SELECT DISTINCT 'afgpolygon' AS table, gkoid, kortnavn " \
              "FROM gvkort.afgpolygon WHERE " \
              "ST_Intersects(geom, ST_MakeEnvelope({},{},{},{},{})) " \
              "UNION ALL " \
              "SELECT DISTINCT 'modelomrhydrologisk' " \
              "AS table, gkoid, kortnavn " \
              "FROM gvkort.modelomrhydrologisk " \
              "WHERE ST_Intersects(geom, " \
              "ST_MakeEnvelope({},{},{},{},{}));".format(
            x1, y1, x2, y2, 25832,
            x1, y1, x2, y2, 25832)

        # GrukosAux.log_info(sql)

        cur = self.execute_sql(sql)
        rows = cur.fetchall()

        # GrukosAux.log_info(rows)

        gkoids = []
        for row in rows:
            # GrukosAux.log_info(row[1])

            if row[1] not in gkoids:
                gkoids.append(row[1])

        # GrukosAux.log_info('gkoids unique'.format(gkoids))

        # We have a list of gkoid: Search the chosen deeplinklayers for an alphanumeric match
        result = []
        for layer in deeplinklayers:
            sql = "SELECT DISTINCT '{}' AS tab, gkoid FROM gvkort.{} WHERE gkoid = any( array{}::integer[]);".format(
                layer, layer, gkoids)
            # GrukosAux.msg_box(sql)
            cur = self.execute_sql(sql)
            rows = cur.fetchall()

            # GrukosAux.log_info(rows)

            for row in rows:
                gkoid = row[1]
                kortnavn = self.gko_gkoid_to_kortnavn(gkoid)

                result.append('{} ({} - {})'.format(layer, gkoid, kortnavn))  # Only place where result is formated...

        return result

    def gko_gkoid_to_kortnavn(self, gkoid):
        """ Get kortnavn from gkoid
        :param gkoid: id to convert to kortnavn
        :return: kortnavn matching gkoid
        """
        sql = 'SELECT gvkort.gko_gkoid_to_kortnavn({});'.format(gkoid)
        cur = self.execute_sql(sql)
        rows_tuple = cur.fetchone()
        kortnavn = rows_tuple[0]

        return kortnavn

    def gko_kortnavn_to_gkoid(self, kortnavn):
        """ Get gkoid from kortnavn"""
        sql = "SELECT gvkort.gko_kortnavn_to_gkoid('{}');".format(kortnavn.title())
        GrukosAux.log_info(sql)
        cur = self.execute_sql(sql)
        rows_tuple = cur.fetchone()
        gkoid = rows_tuple[0]

        return gkoid

    @staticmethod
    def get_userrole():
        return GrukosUser.READER

    def get_dbparams(self):
        # Get db connections params #
        DB = 'grukos'
        PORT = 5432
        HOST = '10.33.131.50'

        user = self.get_userrole()

        if user == GrukosUser.LOCALHOST:
            db_params = {
                'dbname': DB,
                'user': 'postgres',
                'host': 'localhost',
                'password': 'pgnst',
                'port': PORT
            }
            return db_params
        elif user == GrukosUser.READER:
            db_params = {
                'dbname': DB,
                'user': 'grukosreader',
                'host': HOST,
                'password': base64.b64decode('Z2VicjQ3MG5l').decode('ascii'),
                'port': PORT
            }
            return db_params
        else:
            return None

    def gko_generic_search(self, searchstr):
        """ Find gkos matching json template and searchstr  """
        # get search_def.json and query all layers for the text in searchstr
        sqls = self.build_sql_from_json_template(searchstr)
        GrukosAux.log_info('\n' + '\n'.join(sqls))

        gkoids = self.gko_run_sqlarray(sqls)

        if not gkoids:
            return None

        GrukosAux.log_info('\n' + '\n'.join(map(str, gkoids)))
        return gkoids

    def gko_run_sqlarray(self, arr_sql):
        """ Takes an array of sql returns a list of unique gkoids """
        gkoids = set()
        for sql in arr_sql:
            # GrukosAux.log_info(f'Running SQL: {sql}')
            cur = self.execute_sql(sql)

            if not cur:
                GrukosAux.log_info(f'Skipping invalid SQL: {sql}')
                continue  # skip invalide sql like where gkoid=langeland

            # When using wildcards more than one record can be returned
            rows_tuple = cur.fetchall()  # DictRow,  is a psycopg2 subclass of a list
            if rows_tuple:
                for row in rows_tuple:
                    gkoid = row[0]
                    gkoids.add(gkoid)
            else:
                continue

            return gkoids

    def build_sql_from_json_template(self, searchstr):
        """ Build search SQL from JSON template and searchstr """
        sqls = []
        with open(os.path.join(os.path.dirname(__file__), 'search_def.json')) as read_file:
            json_template = json.load(read_file)

            for layer in json_template:
                schema = json_template[layer]['schema']
                table = json_template[layer]['table']
                column = json_template[layer]['column']
                datatype = json_template[layer]['type']

                if datatype.lower() == 'int':
                    sql = f'SELECT gkoid FROM {schema}.{table} WHERE {column}={searchstr}'
                elif datatype.lower() == 'str':
                    sql = f"SELECT gkoid FROM {schema}.{table} WHERE {column} ilike '{searchstr}'"
                else:
                    raise RuntimeError(f'Datatype {datatype.toLower()} for {layer} in search_def.json not support :)')

                sqls.append(sql)

        return sqls
####### new search func start #########
