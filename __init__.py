# -*- coding: utf-8 -*-

# noinspection PyPep8Naming
def classFactory(iface):  # pylint: disable=invalid-name
    """Load Grukos class from file Grukos.

    :param iface: A QGIS interface instance.
    :type iface: QgsInterface
    """
    from .grukos import Grukos
    return Grukos(iface)
