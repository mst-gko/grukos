# -*- coding: utf-8 -*-
from __future__ import absolute_import
from qgis.PyQt.QtCore import pyqtSignal, Qt
from qgis.PyQt.QtGui import QColor, QCursor
from qgis.PyQt.QtWidgets import QApplication, QTreeWidgetItemIterator

from qgis.PyQt import QtGui, uic, QtCore

import qgis
from qgis.core import QgsGeometry, QgsPointXY, QgsRectangle, Qgis, \
        QgsWkbTypes, QgsCoordinateReferenceSystem, QgsCoordinateTransform, \
        QgsProject
from qgis.gui import QgsMapToolEmitPoint, QgsRubberBand
from .grukos_db import *
from .grukos_qgis import *

class RectangleMapTool(QgsMapToolEmitPoint):
    def __init__(self, iface, listView, treeWidget):
        #TODO iface need by grukos_qgis for getting treeWidget selected layers
        #TODO try to use get_dblayers method in RectangleMapTool instead of passing treeWidget
        self.iface = iface
        self.canvas = iface.mapCanvas()
        self.listView = listView
        self.treeWidget = treeWidget

        QgsMapToolEmitPoint.__init__(self, self.canvas)

        self.rubberBand = QgsRubberBand(self.canvas,
                                        QgsWkbTypes.PolygonGeometry)
        self.rubberBand.setColor(Qt.red)
        self.rubberBand.setOpacity(0.25)
        self.rubberBand.setWidth(1)

        self.reset()


    def reset(self):
        self.startPoint = self.endPoint = None
        self.isEmittingPoint = False
        self.rubberBand.reset(QgsWkbTypes.PolygonGeometry)

    def canvasPressEvent(self, e):
        self.startPoint = self.toMapCoordinates(e.pos())
        self.endPoint = self.startPoint
        self.isEmittingPoint = True
        self.showRect(self.startPoint, self.endPoint)

    def clear_listView(self):
        """ Clear items in listView """
        model = self.listView.model()
        if model and model.rowCount() > 0:
            model.removeRows(0, model.rowCount())

        #self.canvas.scene().removeItem(self.rubberBand)

    def canvasReleaseEvent(self, e):

        self.clear_listView()

        self.isEmittingPoint = False
        r = self.rectangle()
        if r is not None:
            #print "Rectangle:", r.xMinimum(), r.yMinimum(), r.xMaximum(), r.yMaximum()
            db = GrukosDb()
            # Rectangle in QGIS is from lower left to upper right
            # Rectangle in PostGIS is from upper left to lower right

            tree_layers = self.get_dbchecked_layers()

            # Split link_layers and db_layers from tree-layers
            link_layers = [d for d in tree_layers if 'deeplink' in d]
            db_layers = [d for d in tree_layers if 'deeplink' not in d]

            if len(db_layers) == 0 and len(link_layers) == 0:
                GrukosAux.msg_box("Der er ikke valgt nogle temaer i søgelisten.")
                return

            table_result = []
            if len(link_layers) > 0:
                #GrukosAux.msg_box('GRUKOS understøtter p.t. ikke link søgning via geografisk bound opslag!')
                table_result = db.gko_bound_partsearch_deeplink(r.xMinimum(), r.yMaximum(), r.xMaximum(), r.yMinimum(), link_layers)

            QApplication.setOverrideCursor(QCursor(Qt.WaitCursor))

            table_result += db.gko_bound_partsearch(r.xMinimum(), r.yMaximum(), r.xMaximum(), r.yMinimum(), db_layers)
            #GrukosAux.msg_box('table_result: {}'.format(table_result))

            # Add tables to listView
            if len(table_result) > 0:
                model = QtGui.QStandardItemModel(self.listView)
                for t in table_result:
                    #GrukosAux.msg_box('t: {}'.format(t))
                    item = QtGui.QStandardItem(t)
                    model.appendRow(item)
                    self.listView.setModel(model)
            else:
                GrukosAux.msg_box(u'Søgningen giver ingen resultater.')

            QApplication.restoreOverrideCursor()

    def get_dbchecked_layers(self):
        # Closure function
        def get_checked_layers():
            """ Get a list of layers checked by the user """
            it = QTreeWidgetItemIterator(self.treeWidget, QTreeWidgetItemIterator.All)
            layers = []
            while it.value():
                item = it.value()
                # Do not check parent items (group text)
                if item.parent() is not None:
                    if item.checkState(0) == QtCore.Qt.Checked:
                        layers.append(item.text(0))
                it += 1

            return layers

        """ Return list of database layers checked in gui """
        gui_layers = get_checked_layers()

        # remove non spatial layers
        spatial_layers = [g for g in gui_layers if g not in ['Projekttabel','NIRAS kommentar',u'Rambøll kommentar']]

        qgrukos = GrukosQgis(self.iface)
        db_layers = qgrukos.guilayername2dblayername(spatial_layers)

        #print 'gui_layers checked: ', gui_layers
        #print 'db_layers checked: ', db_layers

        return db_layers


    def canvasMoveEvent(self, e):
        if not self.isEmittingPoint:
            return

        self.endPoint = self.toMapCoordinates(e.pos())
        self.showRect(self.startPoint, self.endPoint)


    def showRect(self, startPoint, endPoint):
        self.rubberBand.reset(QgsWkbTypes.PolygonGeometry)

        if startPoint.x() == endPoint.x() or startPoint.y() == endPoint.y():
            return

        point1 = QgsPointXY(startPoint.x(), startPoint.y())
        point2 = QgsPointXY(startPoint.x(), endPoint.y())
        point3 = QgsPointXY(endPoint.x(), endPoint.y())
        point4 = QgsPointXY(endPoint.x(), startPoint.y())

        self.rubberBand.addPoint(point1, False)
        self.rubberBand.addPoint(point2, False)
        self.rubberBand.addPoint(point3, False)
        self.rubberBand.addPoint(point4, True)    # true to update canvas
        self.rubberBand.show()


    def rectangle(self):
        if self.startPoint is None or self.endPoint is None:
            return None
        elif self.startPoint.x() == self.endPoint.x() or self.startPoint.y() == self.endPoint.y():
            return None

        r = QgsRectangle(self.startPoint, self.endPoint)

        # Get coordinate reference system value of currently selected layer
        crs_layer = qgis.utils.iface.activeLayer().crs()

        # Transform to EPSG:25832
        crs_dest  = QgsCoordinateReferenceSystem(25832)
        transform = QgsCoordinateTransform(crs_layer, crs_dest,
                                           QgsProject.instance())
        r_transformed = transform.transformBoundingBox(r)

        return r_transformed

    def deactivate(self):
        self.canvas.scene().removeItem(self.rubberBand)
        if RectangleMapTool:
            super(RectangleMapTool, self).deactivate()
            self.deactivated.emit()
