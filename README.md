# QGIS plugin for GRUKOS

[![pipeline status](https://gitlab.com/mst-gko/grukos/badges/master/pipeline.svg)](https://gitlab.com/mst-gko/grukos/commits/master)

This plugin adds functionality to QGIS for interacting with the 
[MST-GKO](https://mst.dk/gko) groundwater database GRUKOS.

The **Stable installation** is recommended for most users, while the 
**Development installation** is required for developers looking to contribute 
code.

## Stable installation
Open the *QGIS* menu *Manage and Install Plugins* and search for `grukos`. 
Press the *Install Plugin* button.

## Development installation
Locate the *Active Profile Folder* for your QGIS3 installation. In the menu, 
select *Settings* > *User Profile* > *Open Active Profile Folder* (or, in 
Danish: *Indstillinger* > *Brugerprofiler* > *Åbn Aktiv Profilmappe*). In this 
directory, create the subdirectories *python\plugins* if they do not already 
exists. The resulting path will on Windows be similar to the following:

    C:\Users\<user>\AppData\Roaming\QGIS\QGIS3\profiles\default\python\plugins\

Use [Git](https://git-scm.com/download/win) to clone the *master* branch of the 
[GRUKOS repository on GitLab](https://gitlab.com/mst-gko/grukos):

    git clone git@gitlab.com:mst-gko/grukos.git

If [SSH](https://en.wikipedia.org/wiki/Secure_Shell) is not configured on the 
machine, cloning can also be done with HTTPS:

    git clone https://gitlab.com/mst-gko/grukos.git

The local copy of the repository can be kept up-to-date with the [grukos master 
branch](https://gitlab.com/mst-gko/grukos/commits/master) with the command:

    git pull

If you would like to contribute, please [fork the repository to your own GitLab 
user](https://gitlab.com/mst-gko/grukos/forks/new), checkout a new development 
branch with an appropriate name, clone it to your local computer, do code 
changes, commit and push them back to your GitLab repository, and open a merge 
request to this repository.

(Re)start QGIS3, and the plugin should be available in the *Manage Plugins* 
tool.

## Publishing a new version
Stable versions of this plugin are automatically uploaded to 
[plugins.qgis.org/plugins/grukos/](https://plugins.qgis.org/plugins/grukos/) 
using a GitLab [pipeline](https://gitlab.com/mst-gko/grukos/pipelines). The 
pipeline is triggered as soon as a new release is tagged. This can be done 
through the [GitLab tag page](https://gitlab.com/mst-gko/grukos/tags), or 
directly from the command line, for example:

    git tag -a vXX.YY.ZZ -m 'Various changes and bugfixes'
    git push origin vXX.YY.ZZ

where `XX.YY.ZZ` denote the new version number, following the [semantic 
versioning scheme](https://semver.org). The OSGeo credentials, used when 
uploading the plugin, are stored as protected variables in [the repository 
settings](https://gitlab.com/mst-gko/grukos/settings/ci_cd) and are used in the 
deploy pipeline.

For now, a copy of the stable release is kept on the MST network drive `F:\`. 
To update the local QGIS2 and QGIS3 versions, run the script 
`update_F_clones.sh` on a machine connected to the MST network.
