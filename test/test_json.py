''' No unittest just for IDE debugging '''
import os
import json

#file = r'C:\Users\b050899\AppData\Roaming\QGIS\QGIS3\profiles\default\python\plugins\grukos\\search_def.json'
#with open(os.path.join(os.path.dirname(__file__), file)) as read_file:
#json_template = json.load(read_file)

my_json = """
{
  "gkoid": {
    "schema": "gvkort",
    "table": "gkoprojekt",
    "column": "gkoid",
    "type":   "int"
  },
  "sagsnavn": {
    "schema": "gvkort",
    "table": "gkoprojekt",
    "column": "sagsnavn",
    "type":   "str"
  },
  "anlaegsnummer": {
    "schema": "gvkort",
    "table": "iol_alle",
    "column": "anlaegsno",
    "type":   "str"
  },
  "vandvaerksnavn": {
    "schema": "gvkort",
    "table": "iol_alle",
    "column": "anl_navn",
    "type":   "str"
  }
}
"""
json_template = json.loads(my_json)

for layer in json_template:
    print(f'layer: {layer} {json_template[layer]}')
    print(f'schema: {json_template[layer]["schema"]}')

exit()

for key, value in json_template.items():
    print('........')
    print(f'key: {key}')
    print(f'value: {value}')

    for key, value in value.items():
        print(f'key: {key}')
        print(f'value: {value}')